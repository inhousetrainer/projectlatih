TUGAS TEST JENKINS (CONTINUES INTEGRATION & CONTINUES DELIVERY)
untuk clone project : git clone https://gitlab.com/inhousetrainer/projectlatih
============================================================
1.	PM MEMBUAT USER USER DEVELOPER UNTUK SETIAP PESERTA DI JENKINS SERVER

2.	PM MEMBUAT PROJECT BARU DI GITLAB DENGAN NAMA PROJECTLATIH

3.	PM MENAMBAHKAN CONTRIBUTOR USER PESERTA DI GITLAB AGAR BISA PUSH PULL

4.	PM MEMBUATKAN TOKEN AGAR SETIAP PESERTA (BUILD) BISA MENARIK GITLAB KE JENKINS

5.	PM MEMBUAT FOLDER FOLDER UNTUK BUILD TEAM DI SERVER PROJECT (GITLAB) AGAR PESERTA BISA MENGERJAKAN BAGIANNYA MASING MASING BERUPA FILE JAVA

6.	PM MEMBAGI 4 KELOMPOK PESERTA (BUILD , DEPLOY, TEST, RELEASE,)

7.	PM MENUGASKAN PESERTA MEMBUAT 4 JOB (LATIHBUILD, LATIHDEPLOY, LATIHTEST, LATIH RELEASE)

8.	SETIAP KELOMPOK PESERTA MENDAPAT PERAN MASING MASING yang diterjemahkan menjadi :
LATIHBUILD : MEMBUAT SCRIPT JAVA MELALUI UBUNTU DISIMPAN DENGAN NAMA Latih1.java , Latih2.java (sesuai member build) + Setup integrasi ke Git + Post Build (LATIHDEPLOY) + buat schedule + poll SCM setiap 2 menit 
LATIHDEPLOY : MENGCOMPILE SCRIPT dengan javac latih1.java, javac latih2.java +  BeforeBuild (LATIHBuild) + Post Build (latihTEST)
LATIHTEST: MENGETEST  SCRIPT dengan java latih1, java latih2  + beforebuild (LATIHDEPLOY) + Post Build ( latihrelease)
LATIHRELEASE: MENAMPILKAN PESAN �RELEASE SUCCESS�  + beforebuild(LATIHTEST)

9.	JALANKAN LATIHBUILD
